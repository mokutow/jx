import X from 'xlsx';
import err from './error.js';

export default class Converter {
    toJSON(wb, cb) {
        let result = {};
        let names = [];
        if (!wb.SheetNames || wb.SheetNames.length == 0) {
            cb(err('JxCommon', 'C1', 'Sheets are empty.'));
            return;
        }
        try {
            wb.SheetNames.forEach(function(name) {
                names.push(name);
                let roa = X.utils.sheet_to_row_object_array(wb.Sheets[name]);
                if (roa.length > 0) {
                    result[name] = roa;
                }
            });
        } catch (e) {
            cb(err('JxCommon', 'C2', 'Sheet to row object conversion error.'));
            return;
        }
        let data = {
            json: result,
            sheets: names
        };
        cb(data);
    }
    toXLSX(json) {
        let wb = {
            SheetNames: [],
            Sheets: {}
        }
        try {
            Object.keys(json).forEach(function(name) {
                wb.SheetNames.push(name);
                let header = {};
                let data = json[name];
                Object.keys(json[name][0]).forEach(function(key) {
                    header[key] = key;
                })
                data.unshift(header);
                let arrays = arraysFromJsonRows(data);
                let ws = sheetFromArrays(arrays);
                wb.Sheets[name] = ws;
            })
        } catch (e) {
            return err('JxCommon', 'C3', 'JSON to XLSX conversion error.');
        }
        return wb;
    }
}

function arraysFromJsonRows(array) {
    let result = [];
    array.forEach(function(obj) {
        let inner = [];
        Object.keys(obj).forEach(function(key) {
            inner.push(obj[key])
        })
        result.push(inner);
    });
    return result;
}

function sheetFromArrays(data, opts) {
    let ws = {};
    let range = {
        s: {
            c: 10000000,
            r: 10000000
        },
        e: {
            c: 0,
            r: 0
        }
    };

    for (let R = 0; R != data.length; ++R) {
        for (let C = 0; C != data[R].length; ++C) {
            if (range.s.r > R) range.s.r = R;
            if (range.s.c > C) range.s.c = C;
            if (range.e.r < R) range.e.r = R;
            if (range.e.c < C) range.e.c = C;
            let cell = {
                v: data[R][C]
            };
            if (cell.v == null) continue;
            let cell_ref = X.utils.encode_cell({
                c: C,
                r: R
            });

            if (typeof cell.v === 'number') cell.t = 'n';
            else if (typeof cell.v === 'boolean') cell.t = 'b';
            else if (cell.v instanceof Date) {
                cell.t = 'n';
                cell.z = X.SSF._table[14];
                cell.v = datenum(cell.v);
            } else cell.t = 's';

            ws[cell_ref] = cell;
        }
    }
    if (range.s.c < 10000000) ws['!ref'] = X.utils.encode_range(range);
    return ws;
}