import Converter from '../common/index.js';
import err from '../common/error.js';
import FileSaver from 'file-saver';
import X from 'xlsx';

export default class JX {
    constructor() {
        this.converter = new Converter();
    }

    toJSON(file, cb) {
        if (!FileReader) {
            cb(err('JxWeb', 'W1', 'Sorry, but your browser does not support FileReader. Check http://caniuse.com/#feat=filereader to check supported platforms.'));
            return;
        }
        let reader = new FileReader();
        reader.onload = (e) => {
            let bt;
            try {
                let data = btoa(fixdata(e.target.result));
                bt = X.read(data, { type: 'base64' });
            } catch (e) {
                cb(err('JxWeb', 'W2', 'Error during file reading.'))
                return;
            }
            this.converter.toJSON(bt, cb);
        };
        reader.onerror = (e) => {
            cb(err('JxWeb', 'W3', 'Error during file reading.'))
        }
        reader.readAsArrayBuffer(file);
    }

    toXLSX(json, filename = "file") {
        let wb = this.converter.toXLSX(json);
        if (wb instanceof Error) {
            return wb;
        }
        let writeOptions = {
            bookType: 'xlsx',
            bookSST: false,
            type: 'binary'
        };
        let blobOptions = {
            type: ""
        }
        let blob;
        try {
            let wbout = X.write(wb, writeOptions);
            blob = new Blob([s2ab(wbout)], blobOptions);
        } catch (e) {
            return err('JxWeb', 'W4', 'Error during file writing.');
        }
        FileSaver.saveAs(blob, filename + ".xlsx")
    }
}

function fixdata(data) {
    let o = "";
    let l = 0;
    let w = 10240;
    for (; l < data.byteLength / w; ++l) {
        o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w, l * w + w)));
    }
    o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w)));
    return o;
}

function s2ab(s) {
    let buf = new ArrayBuffer(s.length);
    let view = new Uint8Array(buf);
    for (let i = 0; i != s.length; ++i) {
        view[i] = s.charCodeAt(i) & 0xFF;
    }
    return buf;
}