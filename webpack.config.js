var webpack = require('webpack');
var UglifyJsPlugin = webpack.optimize.UglifyJsPlugin;
var path = require('path');
var fs = require('fs');
var nodeModules = {};
var env = require('yargs').argv.mode;

fs.readdirSync(path.resolve(__dirname, 'node_modules'))
    .filter(x => ['.bin'].indexOf(x) === -1)
    .forEach(mod => {
        nodeModules[mod] = `commonjs ${mod}`;
    });

var library = 'JX';

var plugins = [],
    outputFile;

if (env === 'build') {
    plugins.push(new UglifyJsPlugin({
        minimize: true
    }));
    outputFile = library + '.min.js';
} else {
    outputFile = library + '.js';
}

var webConfig = {
    name: 'web',
    entry: {
        'web': __dirname + '/src/web/index.js'
    },
    devtool: 'source-map',
    output: {
        path: __dirname + '/dist',
        filename: 'jx-[name].js',
        library: library,
        libraryTarget: 'umd',
        umdNamedDefine: true
    },
    module: {
        loaders: [{
            test: /(\.js)$/,
            loader: 'babel',
            exclude: /(node_modules)/
        }]
    },
    resolve: {
        root: path.resolve('./src'),
        extensions: ['', '.js']
    },
    plugins: plugins,
    watch: true
};

var nodeConfig = {
    name: 'node',
    entry: {
        'node': __dirname + '/src/node/index.js'
    },
    devtool: 'source-map',
    output: {
        path: __dirname + '/dist',
        filename: 'jx-[name].js',
        library: library,
        libraryTarget: 'umd',
        umdNamedDefine: true
    },
    module: {
        loaders: [{
            test: /(\.js)$/,
            loader: 'babel',
            exclude: /(node_modules)/
        }]
    },
    externals: nodeModules,
    resolve: {
        root: path.resolve('./src'),
        extensions: ['', '.js']
    },
    plugins: plugins,
    watch: true
};

module.exports = [
    webConfig,
    nodeConfig
]