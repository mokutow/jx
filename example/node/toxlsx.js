var jx = new(require('../../dist/jx-node.js'))();

var single = {
    'name': [{
        'header one': 1,
        'header two': 2,
        'header three': 3
    }, {
        'header one': 4,
        'header two': 5,
        'header three': 6
    }]
}

var multiple = {
    'name': [{
        'header one': 1,
        'header two': 2,
        'header three': 3
    }, {
        'header one': 4,
        'header two': 5,
        'header three': 6
    }],
    'name two': [{
        'Name': 'Andrew',
        'Number': 331
    }, {
        'Name': 'Jenifer',
        'Number': 555
    }]
}


var s = jx.toXLSX(single, 'result/single');
if (s instanceof Error) {
    console.log(s.description);
}
var m = jx.toXLSX(multiple, 'result/multiple');
if (m instanceof Error) {
    console.log(s.description);
}