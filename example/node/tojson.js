var jx = new(require('../../dist/jx-node.js'))();

jx.toJSON('../assets/simple.xlsx', cb);

function cb(result) {
    if (result instanceof Error) {
        console.log(result);
    } else {
        result.sheets.forEach(function(name) {
            result.json[name].forEach(function(obj) {
                console.log(obj);
            })
        })
    }
}